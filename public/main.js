//ehrlich gesagt msuste ich den ersten Teil kurz spicken. Ich habe nicht verstanden wie ich die Dateien einlesen wollte. Ich habe mehrere Stunden gesucht, wie ich mit Javascript
//Verzichnisse einlesen kann und dann die Dateien dafür verwenden kann.


const displayedImage = document.querySelector('.displayed-img');
const thumbBar = document.querySelector('.thumb-bar');

const btn = document.querySelector('button');
const overlay = document.querySelector('.overlay');

/* Looping through images */

for(let i =1; i<=5; i++){
const newImage = document.createElement('img');
newImage.setAttribute('src', "images/pic" + i + ".jpg");
thumbBar.appendChild(newImage);
newImage.onclick = function(a){
	displayedImage.src = a.target.src;
}

}

/* Wiring up the Darken/Lighten button */

btn.onclick = function(){
let attr = btn.getAttribute("class")

if (attr === "dark"){
	btn.setAttribute("class", "light")
	displayedImage.setAttribute("style", "filter : brightness(50%);")
	btn.innerHTML = "Lighten"
}

else{
	btn.setAttribute("class", "dark")
	displayedImage.setAttribute("style", "filter : brightness(100%);")
	btn.innerHTML = "Darken"

}
}

